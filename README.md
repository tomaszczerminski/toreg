# Toreg
## Introduction
Server uses both HTTP and WS protocols in order to maintain communication with game clients.  
It was initially part of my graduation project.
## Requirements
- Go 1.14 
- Minikube
- Helm
## Setup instructions
As for now, there are no additional steps required in order to run the server.