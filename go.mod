module github.com/tomaszczerminski/toreg

go 1.14

require (
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/google/uuid v1.1.1
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e
	github.com/gorilla/websocket v1.4.2
	github.com/onsi/ginkgo v1.13.0 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.6.1
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
