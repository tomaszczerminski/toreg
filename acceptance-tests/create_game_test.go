package acceptance_tests

import (
	"github.com/google/uuid"
	"github.com/tomaszczerminski/toreg/acceptance-tests/steps"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"testing"
)

func TestCreateGame(t *testing.T) {
	loginRsp := steps.Login(t, &endpoints.LoginRequest{
		Username: uuid.New().String(),
		Password: uuid.New().String(),
	})
	createRoomRsp := steps.CreateRoom(t, &endpoints.CreateRoomRequest{
		PlayerId: loginRsp.PlayerId,
		BoardId:  "841f0f85-0dbe-495c-8375-7d71ac480818",
	})
	availableFractionsRsp := steps.GetAvailableFractions(t)
	_ = steps.ChangeFraction(t, &endpoints.ChangeFractionRequest{
		PlayerID:   loginRsp.PlayerId,
		FractionID: availableFractionsRsp.Fractions[0].Id,
		RoomID:     createRoomRsp.RoomId,
	})
	_ = steps.CreateGame(t, &endpoints.CreateGameRequest{
		PlayerId: loginRsp.PlayerId,
		RoomId:   createRoomRsp.RoomId,
	})
}
