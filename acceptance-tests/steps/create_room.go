package steps

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"io/ioutil"
	"net/http"
	"testing"
)

func CreateRoom(t *testing.T, request *endpoints.CreateRoomRequest) *endpoints.CreateRoomResponse {
	payload, err := json.Marshal(request)
	require.NoError(t, err)
	req, err := http.NewRequest("POST", "http://localhost:8080/create-room", bytes.NewReader(payload))
	require.NoError(t, err)
	raw, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	payload, err = ioutil.ReadAll(raw.Body)
	require.NoError(t, err)
	rsp := new(endpoints.CreateRoomResponse)
	require.NoError(t, json.Unmarshal(payload, rsp))
	return rsp
}
