package steps

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"io/ioutil"
	"net/http"
	"testing"
)

func GetAvailableFractions(t *testing.T) *endpoints.AvailableFractionsResponse {
	raw, err := http.DefaultClient.Get("http://localhost:8080/available-fractions")
	require.NoError(t, err)
	payload, err := ioutil.ReadAll(raw.Body)
	rsp := new(endpoints.AvailableFractionsResponse)
	require.NoError(t, json.Unmarshal(payload, rsp))
	return rsp
}
