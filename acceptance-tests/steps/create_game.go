package steps

import (
	"bytes"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"io/ioutil"
	"net/http"
	"testing"
)

func CreateGame(t *testing.T, request *endpoints.CreateGameRequest) *endpoints.CreateGameResponse {
	payload, err := json.Marshal(request)
	require.NoError(t, err)
	req, err := http.NewRequest("POST", "http://localhost:8080/create-game", bytes.NewReader(payload))
	require.NoError(t, err)
	raw, err := http.DefaultClient.Do(req)
	require.NoError(t, err)
	rsp := new(endpoints.CreateGameResponse)
	payload, err = ioutil.ReadAll(raw.Body)
	require.NoError(t, json.Unmarshal(payload, rsp))
	return rsp
}
