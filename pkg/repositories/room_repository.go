package repositories

import (
	"context"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
	"time"
)

type RoomRepository interface {
	Add(context.Context, *entities.Room) error
	Remove(context.Context, string) error
	GetById(context.Context, string) (*entities.Room, error)
	GetByPlayerId(context.Context, string) (*entities.Room, error)
	GetAll(context.Context) ([]*entities.Room, error)
}

func NewRoomRepository(ctx context.Context) *InMemoryRoomRepository {
	repository := &InMemoryRoomRepository{
		Cache: make(map[string]*entities.Room),
	}
	go func() {
		logger := log.Ctx(ctx)
		for {
			select {
			case <-ctx.Done():
				logger.Info().Caller().Msg("received exit signal")
				return
			case <-time.After(10 * time.Minute):
				rr, err := repository.GetAll(ctx)
				if err != nil {
					logger.Error().Err(err).Caller().Send()
					continue
				}
				logger.Info().Int("number_of_rooms", len(rr)).Msg("rooms statistics")
			}
		}
	}()
	return repository
}

type InMemoryRoomRepository struct {
	sync.Mutex
	Cache map[string]*entities.Room
}

func (r *InMemoryRoomRepository) Add(_ context.Context, room *entities.Room) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[room.Id] = room
	return nil
}

func (r *InMemoryRoomRepository) Remove(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	delete(r.Cache, id)
	return nil
}

var ErrRoomNotFound = errors.New("room does not exist")

func (r *InMemoryRoomRepository) GetById(_ context.Context, id string) (*entities.Room, error) {
	r.Lock()
	defer r.Unlock()
	room, ok := r.Cache[id]
	if !ok {
		return nil, ErrRoomNotFound
	}
	return room, nil
}

func (r *InMemoryRoomRepository) GetAll(ctx context.Context) ([]*entities.Room, error) {
	r.Lock()
	defer r.Unlock()
	var rooms []*entities.Room
	for _, room := range r.Cache {
		rooms = append(rooms, room)
	}
	return rooms, nil
}

func (r *InMemoryRoomRepository) GetByPlayerId(ctx context.Context, playerId string) (*entities.Room, error) {
	r.Lock()
	defer r.Unlock()
	for _, room := range r.Cache {
		if room.ContainsPlayer(playerId) {
			return room, nil
		}
	}
	return nil, ErrRoomNotFound
}
