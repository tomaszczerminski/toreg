package repositories

import (
	"context"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
	"time"
)

type GameRepository interface {
	Add(context.Context, *entities.Game) error
	GetById(context.Context, string) (*entities.Game, error)
	GetByPlayerId(context.Context, string) (*entities.Game, error)
	GetAll(context.Context) ([]*entities.Game, error)
	Remove(context.Context, string) error
}

func NewGameRepository(ctx context.Context) *InMemoryGameRepository {
	repository := &InMemoryGameRepository{
		Cache: make(map[string]*entities.Game),
	}
	go func() {
		logger := log.Ctx(ctx)
		for {
			select {
			case <-ctx.Done():
				logger.Info().Caller().Msg("received exit signal")
				return
			case <-time.After(10 * time.Minute):
				gg, err := repository.GetAll(ctx)
				if err != nil {
					logger.Error().Err(err).Caller().Send()
					continue
				}
				logger.Info().Int("number_of_games", len(gg)).Msg("games statistics")
			}
		}
	}()
	return repository
}

type InMemoryGameRepository struct {
	sync.Mutex
	Cache map[string]*entities.Game
}

func (r *InMemoryGameRepository) Add(_ context.Context, g *entities.Game) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[g.Id] = g
	return nil
}

var ErrGameNotFound = errors.New("game does not exist")

func (r *InMemoryGameRepository) GetById(_ context.Context, id string) (*entities.Game, error) {
	r.Lock()
	defer r.Unlock()
	g, ok := r.Cache[id]
	if !ok {
		return nil, ErrGameNotFound
	}
	return g, nil
}

func (r *InMemoryGameRepository) GetByPlayerId(ctx context.Context, playerId string) (*entities.Game, error) {
	r.Lock()
	defer r.Unlock()
	for _, g := range r.Cache {
		if g.HasPlayer(playerId) {
			return g, nil
		}
	}
	return nil, ErrGameNotFound
}

func (r *InMemoryGameRepository) GetAll(ctx context.Context) ([]*entities.Game, error) {
	r.Lock()
	defer r.Unlock()
	var gg []*entities.Game
	for _, g := range r.Cache {
		gg = append(gg, g)
	}
	return gg, nil
}

func (r *InMemoryGameRepository) Remove(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	delete(r.Cache, id)
	return nil
}
