package repositories

import (
	"context"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
)

type UserRepository interface {
	Add(context.Context, entities.User) error
	GetByUsername(context.Context, string) (*entities.User, error)
}

type InMemoryUserRepository struct {
	sync.Mutex
	Cache map[string]*entities.User
}

func (r *InMemoryUserRepository) Add(ctx context.Context, u *entities.User) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[u.Username] = u
	return nil
}

func (r *InMemoryUserRepository) GetByUsername(ctx context.Context, username string) (*entities.User, error) {
	r.Lock()
	defer r.Unlock()
	u, ok := r.Cache[username]
	if !ok {
		return nil, nil
	}
	return u, nil
}
