package repositories

import (
	"context"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
	"time"
)

type PlayerRepository interface {
	Remove(context.Context, string) error
	Add(context.Context, *entities.Player) error
	GetAll(context.Context) ([]*entities.Player, error)
	GetById(context.Context, string) (*entities.Player, error)
}

func NewPlayerRepository(ctx context.Context) *InMemoryPlayerRepository {
	repository := &InMemoryPlayerRepository{
		Cache: make(map[string]*entities.Player),
	}
	go func() {
		logger := log.Ctx(ctx)
		for {
			select {
			case <-ctx.Done():
				logger.Info().Caller().Msg("received exit signal")
				return
			case <-time.After(10 * time.Minute):
				pp, err := repository.GetAll(ctx)
				if err != nil {
					logger.Error().Err(err).Caller().Send()
					continue
				}
				logger.Info().Int("number_of_players", len(pp)).Msg("players statistics")
			}
		}
	}()
	return repository
}

type InMemoryPlayerRepository struct {
	sync.Mutex
	Cache map[string]*entities.Player
}

func (r *InMemoryPlayerRepository) Remove(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	delete(r.Cache, id)
	return nil
}

func (r *InMemoryPlayerRepository) Add(_ context.Context, p *entities.Player) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[p.Id] = p
	return nil
}

var ErrPlayerNotFound = errors.New("player with specified id does not exist")

func (r *InMemoryPlayerRepository) GetById(_ context.Context, id string) (*entities.Player, error) {
	r.Lock()
	defer r.Unlock()
	p, ok := r.Cache[id]
	if !ok {
		return nil, ErrPlayerNotFound
	}
	return p, nil
}

func (r *InMemoryPlayerRepository) GetAll(ctx context.Context) ([]*entities.Player, error) {
	r.Lock()
	defer r.Unlock()
	var pp []*entities.Player
	for _, p := range r.Cache {
		pp = append(pp, p)
	}
	return pp, nil
}
