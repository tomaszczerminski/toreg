package repositories

import (
	"context"
	"errors"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"sync"
)

type BoardRepository interface {
	Add(context.Context, blueprints.BoardBlueprint) error
	Get(context.Context, string) (blueprints.BoardBlueprint, error)
	GetAll(context.Context) ([]blueprints.BoardBlueprint, error)
	Remove(context.Context, string) error
}

func NewBoardRepository() *InMemoryBoardRepository {
	return &InMemoryBoardRepository{
		Cache: make(map[string]blueprints.BoardBlueprint),
	}
}

type InMemoryBoardRepository struct {
	sync.Mutex
	Cache map[string]blueprints.BoardBlueprint
}

func (r *InMemoryBoardRepository) Add(_ context.Context, blueprint blueprints.BoardBlueprint) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[blueprint.Id] = blueprint
	return nil
}

var ErrBoardNotFound = errors.New("board does not exist")

func (r *InMemoryBoardRepository) Get(_ context.Context, id string) (blueprints.BoardBlueprint, error) {
	r.Lock()
	defer r.Unlock()
	board, ok := r.Cache[id]
	if !ok {
		return blueprints.BoardBlueprint{}, ErrBoardNotFound
	}
	return board, nil
}

func (r *InMemoryBoardRepository) GetAll(_ context.Context) ([]blueprints.BoardBlueprint, error) {
	r.Lock()
	defer r.Unlock()
	var results []blueprints.BoardBlueprint
	for _, b := range r.Cache {
		players := make([]*blueprints.PlayerSlot, 0)
		for _, p := range b.PlayerSlots {
			units := make([]*blueprints.UnitSlot, 0)
			for _, u := range p.UnitSlots {
				units = append(units, &blueprints.UnitSlot{
					Coordinates: u.Coordinates,
				})
			}
			players = append(players, &blueprints.PlayerSlot{
				Available: true,
				UnitSlots: units,
			})
		}
		b.PlayerSlots = players
		items := make([]*blueprints.ItemSlot, 0)
		for _, i := range b.ItemSlots {
			items = append(items, i)
		}
		b.ItemSlots = items
		tiles := make([]*blueprints.TileSlot, 0)
		for _, t := range b.TileSlots {
			tiles = append(tiles, t)
		}
		b.TileSlots = tiles
		results = append(results, b)
	}
	return results, nil
}

func (r *InMemoryBoardRepository) Remove(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	delete(r.Cache, id)
	return nil
}
