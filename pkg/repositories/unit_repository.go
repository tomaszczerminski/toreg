package repositories

import (
	"context"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
	"time"
)

type UnitRepository interface {
	Add(context.Context, *entities.Unit) error
	GetById(context.Context, string) (*entities.Unit, error)
	GetAll(context.Context) ([]*entities.Unit, error)
	Remove(context.Context, string) error
}

func NewUnitRepository(ctx context.Context) *InMemoryUnitRepository {
	repository := &InMemoryUnitRepository{
		Cache: make(map[string]*entities.Unit),
	}
	go func() {
		logger := log.Ctx(ctx)
		for {
			select {
			case <-ctx.Done():
				logger.Info().Caller().Msg("received exit signal")
				return
			case <-time.After(10 * time.Minute):
				uu, err := repository.GetAll(ctx)
				if err != nil {
					logger.Error().Err(err).Caller().Send()
					continue
				}
				logger.Info().Int("number_of_units", len(uu)).Msg("units statistics")
			}
		}
	}()
	return repository
}

type InMemoryUnitRepository struct {
	sync.Mutex
	Cache map[string]*entities.Unit
}

func (r *InMemoryUnitRepository) Add(_ context.Context, u *entities.Unit) error {
	r.Lock()
	defer r.Unlock()
	r.Cache[u.Id] = u
	return nil
}

var ErrUnitNotFound = errors.New("unit with specified id does not exist")

func (r *InMemoryUnitRepository) GetById(_ context.Context, id string) (*entities.Unit, error) {
	r.Lock()
	defer r.Unlock()
	u, ok := r.Cache[id]
	if !ok {
		return nil, ErrUnitNotFound
	}
	return u, nil
}

func (r *InMemoryUnitRepository) GetAll(_ context.Context) ([]*entities.Unit, error) {
	r.Lock()
	defer r.Unlock()
	var results []*entities.Unit
	for _, u := range r.Cache {
		results = append(results, u)
	}
	return results, nil
}

func (r *InMemoryUnitRepository) Remove(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	delete(r.Cache, id)
	return nil
}
