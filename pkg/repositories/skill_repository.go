package repositories

import (
	"context"
	"errors"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
)

type SkillRepository interface {
	Get(context.Context, string) (*entities.Skill, error)
}

func NewSkillRepository() *InMemorySkillRepository {
	return &InMemorySkillRepository{
		Cache: make(map[string]*entities.Skill),
	}
}

type InMemorySkillRepository struct {
	sync.Mutex
	Cache map[string]*entities.Skill
}

var ErrSkillNotFound = errors.New("error with specified id does not exist")

func (r *InMemorySkillRepository) Get(ctx context.Context, id string) (*entities.Skill, error) {
	r.Lock()
	defer r.Unlock()
	skill, ok := r.Cache[id]
	if !ok {
		return nil, ErrSkillNotFound
	}
	return skill, nil
}