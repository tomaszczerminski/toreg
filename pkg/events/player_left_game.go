package events

type PlayerLeftGameEvent struct {
	PlayerId string `json:"player_id"`
}

func (evt *PlayerLeftGameEvent) Type() byte {
	return PlayerLeftGame
}
