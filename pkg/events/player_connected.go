package events

type PlayerConnectedEvent struct {
	ConnectionId string `json:"connection_id"`
}

func (evt *PlayerConnectedEvent) Type() byte {
	return PlayerConnected
}
