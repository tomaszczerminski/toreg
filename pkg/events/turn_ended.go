package events

type TurnEndedEvent struct {
	PlayerId     string `json:"player_id"`
	NextPlayerId string `json:"next_player_id"`
}

func (evt *TurnEndedEvent) Type() byte {
	return TurnEnded
}
