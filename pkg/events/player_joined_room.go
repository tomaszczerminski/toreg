package events

type PlayerJoinedRoomEvent struct {
	PlayerName  string `json:"player_name"`
	PlayerLevel int    `json:"player_level"`
	PlayerID    string `json:"player_id"`
	RoomID      string `json:"room_id"`
}

func (evt *PlayerJoinedRoomEvent) Type() byte {
	return PlayerJoinedRoom
}
