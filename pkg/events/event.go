package events

import "github.com/tomaszczerminski/toreg/pkg/blueprints"

const (
	PlayerConnected = iota
	GameCreated
	PlayerJoinedRoom
	PlayerLeftRoom
	PlayerLeftGame
	PlayerChangedFraction
	PlayerMarkedReady
	UnitMoved
	SkillUsed
	UnitDied
	TurnEnded
)

func Events() []blueprints.EventBlueprint {
	return []blueprints.EventBlueprint{
		{
			EventType: PlayerConnected,
			EventName: "PlayerConnectedEvent",
		},
		{
			EventType: GameCreated,
			EventName: "GameCreatedEvent",
		},
		{
			EventType: PlayerJoinedRoom,
			EventName: "PlayerJoinedRoomEvent",
		},
		{
			EventType: PlayerLeftRoom,
			EventName: "PlayerLeftRoomEvent",
		},
		{
			EventType: PlayerLeftGame,
			EventName: "PlayerLeftGameEvent",
		},
		{
			EventType: PlayerChangedFraction,
			EventName: "PlayerChangedFractionEvent",
		},
		{
			EventType: PlayerMarkedReady,
			EventName: "PlayerMarkedReadyEvent",
		},
		{
			EventType: UnitMoved,
			EventName: "UnitMovedEvent",
		},
		{
			EventType: SkillUsed,
			EventName: "SkillUsedEvent",
		},
		{
			EventType: UnitDied,
			EventName: "UnitDiedEvent",
		},
		{
			EventType: TurnEnded,
			EventName: "TurnEndedEvent",
		},
	}
}

type Event interface {
	Type() byte
}
