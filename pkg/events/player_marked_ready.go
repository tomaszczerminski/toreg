package events

type PlayerMarkedReadyEvent struct {
	PlayerID string `json:"player_id"`
}

func (evt *PlayerMarkedReadyEvent) Type() byte {
	return PlayerMarkedReady
}
