package events

import "github.com/tomaszczerminski/toreg/pkg/values"

type UnitMovedEvent struct {
	UnitId string               `json:"unit_id"`
	Path   []values.Coordinates `json:"path"`
}

func (evt *UnitMovedEvent) Type() byte {
	return UnitMoved
}
