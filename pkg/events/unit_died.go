package events

type UnitDiedEvent struct {
	UnitId string `json:"unit_id"`
}

func (evt *UnitDiedEvent) Type() byte {
	return UnitDied
}
