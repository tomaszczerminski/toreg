package events

type SkillUsedEvent struct {
	UnitId       string `json:"unit_id"`
	TargetUnitId string `json:"target_unit_id"`
	Damage       int    `json:"damage"`
}

func (evt *SkillUsedEvent) Type() byte {
	return SkillUsed
}
