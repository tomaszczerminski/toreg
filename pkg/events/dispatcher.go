package events

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/caches"
)

type Dispatcher interface {
	Dispatch(context.Context, []string, Event) error
}

func NewDispatcher(cache caches.ConnectionCache) *WebsocketEventDispatcher {
	return &WebsocketEventDispatcher{
		Cache: cache,
	}
}

type WebsocketEventDispatcher struct {
	Cache caches.ConnectionCache
}

func (d *WebsocketEventDispatcher) Dispatch(ctx context.Context, recipients []string, event Event) error {
	logger := log.Ctx(ctx)
	for _, recipient := range recipients {
		conn, err := d.Cache.GetById(ctx, recipient)
		if err != nil {
			return fmt.Errorf("%w: player id - %s", err, recipient)
		}
		payload, err := json.Marshal(event)
		if err != nil {
			return err
		}
		msg := make([]byte, len(payload)+1)
		msg[0] = event.Type()
		for i := 0; i < len(payload); i++ {
			msg[i+1] = payload[i]
		}
		conn.Lock()
		if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
			logger.Error().Err(err).Caller().Send()
		}
		conn.Unlock()
	}
	return nil
}
