package events

import (
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/values"
)

type GameCreatedEvent struct {
	Players []blueprints.PlayerBlueprint `json:"players"`
	Tiles   []values.Tile                `json:"tiles"`
	GameId  string                       `json:"game_id"`
}

func (evt *GameCreatedEvent) Type() byte {
	return GameCreated
}
