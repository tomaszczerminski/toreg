package events

type PlayerLeftRoomEvent struct {
	PlayerId string `json:"player_id"`
}

func (evt *PlayerLeftRoomEvent) Type() byte {
	return PlayerLeftRoom
}
