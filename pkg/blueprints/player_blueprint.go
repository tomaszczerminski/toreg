package blueprints

type PlayerBlueprint struct {
	Id    string          `json:"id"`
	Name  string          `json:"name"`
	Type  byte            `json:"type"`
	Units []UnitBlueprint `json:"units"`
}
