package blueprints

type SkillBlueprint struct {
	Name          string `json:"name"`
	MinimumDamage int    `json:"minimum_damage"`
	MaximumDamage int    `json:"maximum_damage"`
	EnergyCost    int    `json:"energy_cost"`
	Cooldown      int    `json:"cooldown"`
	Sprite        string `json:"sprite"`
}
