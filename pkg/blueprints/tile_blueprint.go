package blueprints

import "github.com/tomaszczerminski/toreg/pkg/values"

type TileBlueprint struct {
	Coordinates   values.Coordinates   `json:"coordinates"`
	TraversalCost int                  `json:"traversal_cost"`
	Sprite        string               `json:"sprite"`
	Neighbours    []values.Coordinates `json:"neighbours"`
}
