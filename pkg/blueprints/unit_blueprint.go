package blueprints

import "github.com/tomaszczerminski/toreg/pkg/values"

type UnitBlueprint struct {
	Id                      string             `json:"id"`
	Name                    string             `json:"name"`
	StartCoordinates        values.Coordinates `json:"start_coordinates"`
	ModelId                 string             `json:"model_id"`
	HealthPoints            int                `json:"health_points"`
	RemainingHealthPoints   int                `json:"remaining_health_points"`
	EnergyPoints            int                `json:"energy_points"`
	RemainingEnergyPoints   int                `json:"remaining_energy_points"`
	MovementPoints          int                `json:"movement_points"`
	RemainingMovementPoints int                `json:"remaining_movement_points"`
	Skill1                  SkillBlueprint     `json:"skill_1"`
	Skill2                  SkillBlueprint     `json:"skill_2"`
	Skill3                  SkillBlueprint     `json:"skill_3"`
	ViewRange               int                `json:"view_range"`
}
