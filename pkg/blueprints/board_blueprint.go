package blueprints

import (
	"github.com/tomaszczerminski/toreg/pkg/values"
)

type UnitSlot struct {
	Coordinates values.Coordinates `json:"coordinates"`
}

type PlayerSlot struct {
	Available bool        `json:"-"`
	UnitSlots []*UnitSlot `json:"unit_slots"`
}

type ItemSlot struct {
	Coordinates values.Coordinates `json:"coordinates"`
	Tier        int                `json:"tier"`
}

type TileSlot struct {
	Sprite        string               `json:"sprite"`
	TraversalCost int                  `json:"traversal_cost"`
	Coordinates   values.Coordinates   `json:"coordinates"`
	Neighbours    []values.Coordinates `json:"neighbours"`
}

type BoardBlueprint struct {
	Id          string        `json:"id"`
	Name        string        `json:"name"`
	Type        string        `json:"type"`
	Size        string        `json:"size"`
	PlayerSlots []*PlayerSlot `json:"player_slots"`
	ItemSlots   []*ItemSlot   `json:"item_slots"`
	TileSlots   []*TileSlot   `json:"tile_slots"`
}
