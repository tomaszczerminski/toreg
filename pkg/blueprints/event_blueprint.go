package blueprints

type EventBlueprint struct {
	EventType byte   `json:"event_type"`
	EventName string `json:"event_name"`
}
