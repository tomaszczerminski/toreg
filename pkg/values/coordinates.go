package values

type Coordinates struct {
	Row    int `json:"row"`
	Column int `json:"column"`
}
