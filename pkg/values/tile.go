package values

type Tile struct {
	GameId        string        `json:"game_id"`
	Coordinates   Coordinates   `json:"coordinates"`
	TraversalCost int           `json:"traversal_cost"`
	Sprite        string        `json:"sprite"`
	Neighbours    []Coordinates `json:"neighbours"`
	IsOccupied    bool          `json:"is_occupied"`
}
