package factories

import (
	"context"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
)

type BoardFactory interface {
	Create(context.Context, string) (*blueprints.BoardBlueprint, error)
}

type DefaultBoardFactory struct {
	Boards repositories.BoardRepository
}

func (bf *DefaultBoardFactory) Create(ctx context.Context, id string) (*blueprints.BoardBlueprint, error) {
	b, err := bf.Boards.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	players := make([]*blueprints.PlayerSlot, len(b.PlayerSlots))
	for i := 0; i < len(b.PlayerSlots); i++ {
		prototype := b.PlayerSlots[i]
		players[i] = &blueprints.PlayerSlot{
			Available: true,
			UnitSlots: prototype.UnitSlots,
		}
	}
	items := make([]*blueprints.ItemSlot, len(b.ItemSlots))
	for i := 0; i < len(b.ItemSlots); i++ {
		prototype := b.ItemSlots[i]
		items[i] = &blueprints.ItemSlot{
			Coordinates: prototype.Coordinates,
			Tier:        prototype.Tier,
		}
	}
	tiles := make([]*blueprints.TileSlot, len(b.TileSlots))
	for i := 0; i < len(b.TileSlots); i++ {
		prototype := b.TileSlots[i]
		tiles[i] = &blueprints.TileSlot{
			Coordinates: prototype.Coordinates,
			Neighbours:  prototype.Neighbours,
			Sprite:      prototype.Sprite,
		}
	}
	return &blueprints.BoardBlueprint{
		Id:              b.Id,
		Name:            b.Name,
		Type:            b.Type,
		PlayerSlots:     players,
		ItemSlots:       items,
		TileSlots:       tiles,
	}, nil
}

