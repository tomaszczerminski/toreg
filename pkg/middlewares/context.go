package middlewares

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func Context() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			logger := log.With().Logger()
			ctx = (&logger).WithContext(ctx)
			playerId := r.Header.Get("X-Player-Id")
			if playerId != "" {
				logger = logger.With().Str("player_id", playerId).Logger()
				ctx = (&logger).WithContext(ctx)
			}
			gameId := r.Header.Get("X-Game-Id")
			if gameId != "" {
				logger = logger.With().Str("game_id", gameId).Logger()
				ctx = (&logger).WithContext(ctx)
			}
			roomId := r.Header.Get("X-Room-Id")
			if roomId != "" {
				logger = logger.With().Str("room_id", roomId).Logger()
				ctx = (&logger).WithContext(ctx)
			}
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
