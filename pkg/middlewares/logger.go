package middlewares

import (
	"bufio"
	"bytes"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net"
	"net/http"
)

type ProxyWriter struct {
	w        http.ResponseWriter
	callback func(status int, rsp string)
	status   int
}

func (pw *ProxyWriter) Write(payload []byte) (int, error) {
	pw.callback(pw.status, string(payload))
	return pw.w.Write(payload)
}

func (pw *ProxyWriter) Header() http.Header {
	return pw.w.Header()
}

func (pw *ProxyWriter) WriteHeader(header int) {
	pw.status = header
	pw.w.WriteHeader(header)
}

func (pw *ProxyWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return pw.w.(http.Hijacker).Hijack()
}

func Logger() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			logger := log.Ctx(ctx)
			payload, err := ioutil.ReadAll(r.Body)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				w.WriteHeader(http.StatusBadRequest)
				_, _ = w.Write([]byte(err.Error()))
				return
			}
			r.Body = ioutil.NopCloser(bytes.NewReader(payload))
			next.ServeHTTP(&ProxyWriter{
				callback: func(status int, rsp string) {
					if len(payload) == 0 {
						payload = []byte("N/A")
					}
					logger.Info().
						Str("http_method", r.Method).
						Str("request_path", r.RequestURI).
						Str("response_body", rsp).
						Int("status_code", status).
						Str("request_body", string(payload)).
						Msg("http request")
				},
				w: w,
			}, r)
		}
		return http.HandlerFunc(fn)

	}
}
