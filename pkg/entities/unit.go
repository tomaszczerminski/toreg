package entities

import (
	"github.com/tomaszczerminski/toreg/pkg/values"
	"sync"
)

type Unit struct {
	sync.Mutex
	Id                      string
	Name                    string
	StartTileCoordinates    values.Coordinates
	CurrentTileCoordinates  values.Coordinates
	HealthPoints            int
	RemainingHealthPoints   int
	EnergyPoints            int
	RemainingEnergyPoints   int
	MovementPoints          int
	RemainingMovementPoints int
	Skill1                  *Skill
	Skill2                  *Skill
	Skill3                  *Skill
}
