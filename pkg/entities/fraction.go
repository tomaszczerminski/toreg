package entities

import "github.com/tomaszczerminski/toreg/pkg/blueprints"

type Fraction struct {
	Id             string                     `json:"id"`
	Name           string                     `json:"name"`
	UnitBlueprints []blueprints.UnitBlueprint `json:"units"`
}
