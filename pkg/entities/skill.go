package entities

type Skill struct {
	Id            string
	Name          string
	MinimumDamage int
	MaximumDamage int
	Sprite        string
}
