package entities

import (
	"context"
	"errors"
	"sync"
)

type PlayerPreference struct {
	PlayerId     string `json:"player_id"`
	FractionId   string `json:"fraction_id"`
}

type Room struct {
	sync.Mutex
	Id          string
	BoardId     string
	Capacity    int
	Preferences []*PlayerPreference
}

func (r *Room) ContainsPlayer(id string) bool {
	for _, entry := range r.Preferences {
		if entry.PlayerId == id {
			return true
		}
	}
	return false
}

var ErrInvalidRoomState = errors.New("room in invalid state")

func (r *Room) Owner() (string, error) {
	r.Lock()
	defer r.Unlock()
	if len(r.Preferences) < 1 {
		return "", ErrInvalidRoomState
	}
	return r.Preferences[0].PlayerId, nil
}

func (r *Room) IsFull() bool {
	r.Lock()
	defer r.Unlock()
	return r.Capacity == len(r.Preferences)
}

func (r *Room) IsEmpty() bool {
	r.Lock()
	defer r.Unlock()
	return len(r.Preferences) == 0
}

func (r *Room) RemovePlayer(_ context.Context, id string) error {
	r.Lock()
	defer r.Unlock()
	players := make([]*PlayerPreference, 0)
	for _, player := range r.Preferences {
		if player.PlayerId == id {
			continue
		}
		players = append(players, player)
	}
	r.Preferences = players
	return nil
}
