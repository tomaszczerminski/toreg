package entities

import (
	"github.com/gorilla/websocket"
	"sync"
)

type Connection struct {
	*sync.Mutex
	*websocket.Conn
	Id string
}
