package entities

type ItemType int

const (
	Weapon ItemType = iota
	Armor
	Shield
)

type Item struct {
	ID      string   `json:"id"`
	Type    ItemType `json:"type"`
	Effects []string `json:"effects"`
}
