package entities

import (
	"context"
	"errors"
	"github.com/tomaszczerminski/toreg/pkg/values"
	"sync"
)

type PlayerRecord struct {
	PlayerId     string
	Units        []string
	HasTurn      bool
	CurrentTurn  int
	NextPlayerId string
}

type Game struct {
	sync.Mutex
	Id      string
	Players []*PlayerRecord
	Tiles   []values.Tile
}

var (
	ErrPlayerRecordNotFound = errors.New("player is not in game")
	ErrInvalidPlayerState   = errors.New("player has invalid state")
	ErrTileNotFound         = errors.New("tile does not exist")
)

func (g *Game) NextTurn(playerId string) (string, int, error) {
	g.Lock()
	defer g.Unlock()
	var record *PlayerRecord
	for _, p := range g.Players {
		if p.PlayerId == playerId {
			record = p
			break
		}
	}
	if record == nil {
		return "", 0, ErrPlayerRecordNotFound
	}
	if !record.HasTurn {
		return "", 0, ErrInvalidPlayerState
	}
	record.HasTurn = false
	record.CurrentTurn++
	var next *PlayerRecord
	for _, p := range g.Players {
		if p.PlayerId == record.NextPlayerId {
			p.HasTurn = true
			next = p
			break
		}
	}
	if next == nil {
		return "", 0, ErrPlayerRecordNotFound
	}
	return next.PlayerId, record.CurrentTurn, nil
}

func (g *Game) GetTileByCoordinates(coordinates values.Coordinates) (values.Tile, error) {
	g.Lock()
	defer g.Unlock()
	for _, t := range g.Tiles {
		if t.Coordinates == coordinates {
			return t, nil
		}
	}
	return values.Tile{}, ErrTileNotFound
}

func (g *Game) CurrentPlayer() string {
	g.Lock()
	defer g.Unlock()
	for _, record := range g.Players {
		if record.HasTurn {
			return record.PlayerId
		}
	}
	return ""
}

func (g *Game) HasPlayer(playerId string) bool {
	g.Lock()
	defer g.Unlock()
	for _, p := range g.Players {
		if p.PlayerId == playerId {
			return true
		}
	}
	return false
}

func (g *Game) RemovePlayer(ctx context.Context, playerId string) error {
	g.Lock()
	defer g.Unlock()
	var target []*PlayerRecord
	for _, player := range g.Players {
		if player.PlayerId == playerId {
			continue
		}
		target = append(target, player)
	}
	g.Players = target
	return nil
}

func (g *Game) IsEmpty() bool {
	g.Lock()
	defer g.Unlock()
	return len(g.Players) == 0
}
