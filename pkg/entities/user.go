package entities

import "time"

type Status byte

const (
	Banned Status = iota
	Active
)

type User struct {
	Email            string
	Username         string
	PasswordHash     []byte
	LastLoginDate    time.Time
	RegistrationDate time.Time
	Status           Status
}
