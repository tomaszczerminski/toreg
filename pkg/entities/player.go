package entities

import (
	"sync"
	"time"
)

type Player struct {
	sync.Mutex
	Id           string
	ConnectionId string
	Name         string
	Level        int
	LastSignIn   time.Time
	SignUpTime   time.Time
}
