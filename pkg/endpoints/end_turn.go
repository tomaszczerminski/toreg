package endpoints

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type EndTurnRequest struct {
	GameId   string `json:"game_id"`
	PlayerId string `json:"player_id"`
}

type EndTurnResponse struct {
	CurrentTurn  int
	NextPlayerId string
}

func EndTurn(games repositories.GameRepository, dispatcher events.Dispatcher) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request EndTurnRequest
		if err := json.Unmarshal(payload, &request); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		game, err := games.GetById(ctx, request.PlayerId)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if game.CurrentPlayer() != request.PlayerId {
			w.WriteHeader(http.StatusConflict)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		nextPlayerId, currentTurn, err := game.NextTurn(request.PlayerId)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		response, err := json.Marshal(&EndTurnResponse{
			CurrentTurn:  currentTurn,
			NextPlayerId: nextPlayerId,
		})
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var recipients []string
		for _, record := range game.Players {
			recipients = append(recipients, record.PlayerId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.TurnEndedEvent{
			PlayerId:     request.PlayerId,
			NextPlayerId: nextPlayerId,
		}); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(response)
	}
}
