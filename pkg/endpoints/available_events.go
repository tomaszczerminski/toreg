package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"net/http"
)



type GetAvailableEventsResponse struct {
	Events []blueprints.EventBlueprint `json:"events"`
}

func GetAvailableEvents() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		payload, err := json.Marshal(&GetAvailableEventsResponse{
			Events: events.Events(),
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(payload)
	}
}
