package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"net/http"
)

type AvailableFractionsResponse struct {
	Fractions []entities.Fraction `json:"fractions"`
}

func GetAvailableFractions(fractions []entities.Fraction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		payload, err := json.Marshal(&AvailableFractionsResponse{
			Fractions: fractions,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(payload)
	}
}
