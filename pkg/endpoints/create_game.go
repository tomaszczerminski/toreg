package endpoints

import (
	"encoding/json"
	"errors"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/factories"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"github.com/tomaszczerminski/toreg/pkg/values"
	"io/ioutil"
	"math/rand"
	"net/http"
)

type CreateGameRequest struct {
	PlayerId string `json:"player_id"`
	RoomId   string `json:"room_id"`
}

type CreateGameResponse struct {
	Players []blueprints.PlayerBlueprint `json:"players"`
	GameId  string                       `json:"game_id"`
}

func CreateGame(
	players repositories.PlayerRepository,
	games repositories.GameRepository,
	units repositories.UnitRepository,
	rooms repositories.RoomRepository,
	fractions []entities.Fraction,
	factory factories.BoardFactory,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		request := new(CreateGameRequest)
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if err := json.Unmarshal(body, request); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room, err := rooms.GetById(ctx, request.RoomId)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		gameId := uuid.New().String()
		board, err := factory.Create(ctx, room.BoardId)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if len(board.PlayerSlots) == 0 {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		tt := make([]values.Tile, 0)
		for _, slot := range board.TileSlots {
			tile := values.Tile{
				GameId:        gameId,
				Coordinates:   slot.Coordinates,
				TraversalCost: slot.TraversalCost,
				Sprite:        slot.Sprite,
				Neighbours:    slot.Neighbours,
			}
			tt = append(tt, tile)
		}
		game := &entities.Game{
			Id:    gameId,
			Tiles: tt,
			Players: []*entities.PlayerRecord{},
		}
		if err := games.Add(ctx, game); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		pp := make([]blueprints.PlayerBlueprint, 0)
		for _, preference := range room.Preferences {
			player, err := players.GetById(ctx, preference.PlayerId)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			var slot *blueprints.PlayerSlot
			for {
				slot = board.PlayerSlots[rand.Intn(len(board.PlayerSlots))]
				if slot.Available {
					slot.Available = false
					break
				}
			}
			var bb []blueprints.UnitBlueprint
			for i, _ := range slot.UnitSlots {
				var found bool
				var fraction entities.Fraction
				for _, it := range fractions {
					if it.Id == preference.FractionId {
						fraction = it
						found = true
						break
					}
				}
				if !found {
					logger.Error().Err(errors.New("fraction does not exist")).Caller().Send()
					w.WriteHeader(http.StatusNotFound)
					_, _ = w.Write(make([]byte, 0))
					return
				}
				blueprint := fraction.UnitBlueprints[i]
				unit := &entities.Unit{
					Id:                      uuid.New().String(),
					Name:                    blueprint.Name,
					StartTileCoordinates:    blueprint.StartCoordinates,
					CurrentTileCoordinates:  blueprint.StartCoordinates,
					HealthPoints:            blueprint.HealthPoints,
					RemainingHealthPoints:   blueprint.HealthPoints,
					EnergyPoints:            blueprint.EnergyPoints,
					RemainingEnergyPoints:   blueprint.EnergyPoints,
					MovementPoints:          blueprint.MovementPoints,
					RemainingMovementPoints: blueprint.MovementPoints,
					Skill1: &entities.Skill{
						MinimumDamage: blueprint.Skill1.MinimumDamage,
						MaximumDamage: blueprint.Skill1.MaximumDamage,
						Sprite:        blueprint.Skill1.Sprite,
					},
					Skill2: &entities.Skill{
						MinimumDamage: blueprint.Skill2.MinimumDamage,
						MaximumDamage: blueprint.Skill2.MaximumDamage,
						Sprite:        blueprint.Skill2.Sprite,
					},
					Skill3: &entities.Skill{
						MinimumDamage: blueprint.Skill3.MinimumDamage,
						MaximumDamage: blueprint.Skill3.MaximumDamage,
						Sprite:        blueprint.Skill3.Sprite,
					},
				}
				blueprint.Id = unit.Id
				blueprint.StartCoordinates = unit.StartTileCoordinates
				blueprint.RemainingHealthPoints = unit.RemainingHealthPoints
				blueprint.RemainingEnergyPoints = unit.RemainingEnergyPoints
				blueprint.RemainingMovementPoints = unit.RemainingMovementPoints
				if err := units.Add(ctx, unit); err != nil {
					logger.Error().Err(err).Caller().Send()
					w.WriteHeader(http.StatusInternalServerError)
					_, _ = w.Write(make([]byte, 0))
					return
				}
				bb = append(bb, blueprint)
			}
			var t byte
			if request.PlayerId == player.Id {
				t = 0
			} else {
				t = 1
			}
			pp = append(pp, blueprints.PlayerBlueprint{
				Id:    player.Id,
				Name:  player.Name,
				Type:  t,
				Units: bb,
			})
		}
		var recipients []string
		for _, preference := range room.Preferences {
			if preference.PlayerId == request.PlayerId {
				continue
			}
			recipient, err := players.GetById(ctx, preference.PlayerId)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			recipients = append(recipients, recipient.ConnectionId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.GameCreatedEvent{
			Players: pp,
			Tiles:   tt,
			GameId:  gameId,
		}); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		rsp, err := json.Marshal(&CreateGameResponse{
			Players: pp,
			GameId:  gameId,
		})
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusCreated)
		_, _ = w.Write(rsp)
	}
}
