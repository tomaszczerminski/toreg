package tests

import (
	"context"
	"github.com/stretchr/testify/mock"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/values"
)

type MockRoomRepository struct {
	mock.Mock
}

func (m *MockRoomRepository) Add(ctx context.Context, r *entities.Room) error {
	args := m.Called(ctx, r)
	return args.Error(0)
}

func (m *MockRoomRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func (m *MockRoomRepository) GetByPlayerId(ctx context.Context, playerId string) (*entities.Room, error) {
	args := m.Called(ctx, playerId)
	return args.Get(0).(*entities.Room), args.Error(1)
}

func (m *MockRoomRepository) GetById(ctx context.Context, id string) (*entities.Room, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entities.Room), args.Error(1)
}

func (m *MockRoomRepository) GetAll(ctx context.Context) ([]*entities.Room, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*entities.Room), args.Error(1)
}

type MockPlayerRepository struct {
	mock.Mock
}

func (m *MockPlayerRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func (m *MockPlayerRepository) Add(ctx context.Context, p *entities.Player) error {
	args := m.Called(ctx, p)
	return args.Error(0)
}

func (m *MockPlayerRepository) GetAll(ctx context.Context) ([]*entities.Player, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*entities.Player), args.Error(1)
}

func (m *MockPlayerRepository) GetById(ctx context.Context, id string) (*entities.Player, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entities.Player), args.Error(1)
}

type MockEventDispatcher struct {
	mock.Mock
}

func (m *MockEventDispatcher) Dispatch(ctx context.Context, recipients []string, evt events.Event) error {
	args := m.Called(ctx, recipients, evt)
	return args.Error(0)
}

type MockGameRepository struct {
	mock.Mock
}

func (m *MockGameRepository) Add(ctx context.Context, g *entities.Game) error {
	args := m.Called(ctx, g)
	return args.Error(0)
}

func (m *MockGameRepository) GetById(ctx context.Context, id string) (*entities.Game, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entities.Game), args.Error(1)
}

func (m *MockGameRepository) GetByPlayerId(ctx context.Context, playerId string) (*entities.Game, error) {
	args := m.Called(ctx, playerId)
	return args.Get(0).(*entities.Game), args.Error(1)
}

func (m *MockGameRepository) GetAll(ctx context.Context) ([]*entities.Game, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*entities.Game), args.Error(1)
}

func (m *MockGameRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

type MockTileRepository struct {
	mock.Mock
}

func (m *MockTileRepository) Add(ctx context.Context, t *values.Tile) error {
	args := m.Called(ctx, t)
	return args.Error(0)
}

func (m *MockTileRepository) GetAll(ctx context.Context) ([]*values.Tile, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*values.Tile), args.Error(1)
}

func (m *MockTileRepository) GetByGameIdAndCoordinates(ctx context.Context, gameId string, coordinates values.Coordinates) (*values.Tile, error) {
	args := m.Called(ctx, gameId, coordinates)
	return args.Get(0).(*values.Tile), args.Error(1)
}

func (m *MockTileRepository) GetById(ctx context.Context, id string) (*values.Tile, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*values.Tile), args.Error(1)
}

func (m *MockTileRepository) GetByCoordinates(ctx context.Context, coordinates values.Coordinates) (*values.Tile, error) {
	args := m.Called(ctx, coordinates)
	return args.Get(0).(*values.Tile), args.Error(1)
}

func (m *MockTileRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

type MockUnitRepository struct {
	mock.Mock
}

func (m *MockUnitRepository) Add(ctx context.Context, u *entities.Unit) error {
	args := m.Called(ctx, u)
	return args.Error(0)
}

func (m *MockUnitRepository) GetAll(ctx context.Context) ([]*entities.Unit, error) {
	args := m.Called(ctx)
	return args.Get(0).([]*entities.Unit), args.Error(1)
}

func (m *MockUnitRepository) GetById(ctx context.Context, id string) (*entities.Unit, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*entities.Unit), args.Error(1)
}

func (m *MockUnitRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

type MockBoardRepository struct {
	mock.Mock
}

func (m *MockBoardRepository) Add(ctx context.Context, blueprint blueprints.BoardBlueprint) error {
	args := m.Called(ctx, blueprint)
	return args.Error(0)
}

func (m *MockBoardRepository) Get(ctx context.Context, id string) (blueprints.BoardBlueprint, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(blueprints.BoardBlueprint), args.Error(1)
}

func (m *MockBoardRepository) GetAll(ctx context.Context) ([]blueprints.BoardBlueprint, error) {
	args := m.Called(ctx)
	return args.Get(0).([]blueprints.BoardBlueprint), args.Error(1)
}

func (m *MockBoardRepository) Remove(ctx context.Context, id string) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

type MockBoardFactory struct {
	mock.Mock
}

func (m *MockBoardFactory) Create(ctx context.Context, id string) (*blueprints.BoardBlueprint, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(*blueprints.BoardBlueprint), args.Error(1)
}