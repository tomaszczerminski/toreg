package tests

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAvailableRoomsTest(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	require.NoError(t, err)
	rooms := new(MockRoomRepository)
	rooms.On("GetAll", mock.Anything).
		Return([]*entities.Room{}, nil).
		Once()
	players := new(MockPlayerRepository)
	boards := new(MockBoardRepository)
	rec := httptest.NewRecorder()
	r := chi.NewMux()
	r.Get("/", endpoints.GetAvailableRooms(rooms, players, boards))
	r.ServeHTTP(rec, req)
	require.Equal(t, http.StatusOK, rec.Code)
}
