package tests

import (
	"bytes"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/values"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCreateGame(t *testing.T) {
	players := new(MockPlayerRepository)
	defer players.AssertExpectations(t)
	players.On("GetById", mock.Anything, mock.Anything).Return(&entities.Player{}, nil)
	games := new(MockGameRepository)
	defer games.AssertExpectations(t)
	games.On("Create", mock.Anything, mock.Anything).Return(nil).Once()
	units := new(MockUnitRepository)
	defer units.AssertExpectations(t)
	rooms := new(MockRoomRepository)
	defer rooms.AssertExpectations(t)
	tiles := new(MockTileRepository)
	tiles.On("GetByCoordinates", mock.Anything, mock.Anything).Return(&values.Tile{

	}, nil)
	players.On("GetById", mock.Anything, mock.Anything).Return(&entities.Player{
		Id:         uuid.New().String(),
		Name:       "PLAYER_1",
		Level:      1,
	}, nil)
	games.On("Add", mock.Anything, mock.Anything).Return(nil)
	units.On("Add", mock.Anything, mock.Anything).Return(nil)
	rooms.On("GetById", mock.Anything, mock.Anything).Return(&entities.Room{
		Preferences: []*entities.PlayerPreference{
			{
				FractionId: "1",
			},
		},
	}, nil)
	fractions := []entities.Fraction{
		{
			Id:   "1",
			Name: uuid.New().String(),
			UnitBlueprints: []blueprints.UnitBlueprint{
				{

				},
			},
		},
	}
	dispatcher := new(MockEventDispatcher)
	dispatcher.On("Dispatch", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	bf := new(MockBoardFactory)
	bf.On("Create", mock.Anything, mock.Anything).Return(&blueprints.BoardBlueprint{
		PlayerSlots: []*blueprints.PlayerSlot{
			{
				Available: true,
			},
			{
				Available: true,
			},
		},
	}, nil)
	r := chi.NewMux()
	r.Post("/", endpoints.CreateGame(players, games, units, rooms, tiles, fractions, bf, dispatcher))
	rec := httptest.NewRecorder()
	payload, err := json.Marshal(&endpoints.CreateGameRequest{
		PlayerId: uuid.New().String(),
		RoomId:   uuid.New().String(),
	})
	require.NoError(t, err)
	req, err := http.NewRequest("POST", "/", bytes.NewReader(payload))
	require.NoError(t, err)
	r.ServeHTTP(rec, req)
	require.Equal(t, http.StatusCreated, rec.Code)
}
