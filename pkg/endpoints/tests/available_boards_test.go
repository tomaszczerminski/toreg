package tests

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetAvailableBoards(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	require.NoError(t, err)
	rec := httptest.NewRecorder()
	boards := new(MockBoardRepository)
	boards.On("GetAll", mock.Anything).Return([]blueprints.BoardBlueprint{
		{
			Name:            "NAME_1",
			Type:            "TYPE_1",
			Size:            "SIZE_1",
		},
	}, nil).Once()
	r := chi.NewMux()
	r.Get("/", endpoints.GetAvailableBoards(boards))
	r.ServeHTTP(rec, req)
	require.Equal(t, http.StatusOK, rec.Code)
	payload, err := ioutil.ReadAll(rec.Body)
	require.NoError(t, err)
	var rsp endpoints.GetAvailableBoardsResponse
	err = json.Unmarshal(payload, &rsp)
	require.NoError(t, err)
	require.Equal(t, 1, len(rsp.Boards))
	board := rsp.Boards[0]
	require.Equal(t, "NAME_1", board.BoardName)
	require.Equal(t, "TYPE_1", board.BoardType)
	require.Equal(t, "SIZE_1", board.BoardSize)
}
