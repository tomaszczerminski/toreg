package tests

import (
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAvailableFractions(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	require.NoError(t, err)
	fractions := []entities.Fraction{
		{
			Id:             uuid.New().String(),
			Name:           "NAME_1",
			UnitBlueprints: []blueprints.UnitBlueprint{},
		},
	}
	rec := httptest.NewRecorder()
	r := chi.NewMux()
	r.Get("/", endpoints.GetAvailableFractions(fractions))
	r.ServeHTTP(rec, req)
	require.Equal(t, http.StatusOK, rec.Code)
}
