package tests

import (
	"bytes"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMarkPlayerReady(t *testing.T) {
	rooms := new(MockRoomRepository)
	rooms.
		On("GetById", mock.Anything, mock.Anything).
		Return(new(entities.Room), nil).
		Once()
	dispatcher := new(MockEventDispatcher)
	dispatcher.
		On("Dispatch", mock.Anything, mock.Anything, mock.Anything).
		Return(nil)
	payload, err := json.Marshal(&endpoints.MarkPlayerReadyRequest{
		PlayerID: uuid.New().String(),
		RoomID:   uuid.New().String(),
	})
	require.NoError(t, err)
	req, err := http.NewRequest("GET", "/", bytes.NewReader(payload))
	require.NoError(t, err)
	rec := httptest.NewRecorder()
	r := chi.NewMux()
	r.Get("/", endpoints.MarkPlayerReady(rooms, dispatcher))
	r.ServeHTTP(rec, req)
}
