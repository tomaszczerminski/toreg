package tests

import (
	"bytes"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/values"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestMoveUnit(t *testing.T) {
	games := new(MockGameRepository)
	games.
		On("GetById", mock.Anything, mock.Anything).
		Return(&entities.Game{
			Players: []*entities.PlayerRecord{
				{
					PlayerId: "PLAYER_1",
				},
				{
					PlayerId: "PLAYER_2",
				},
				{
					PlayerId: "PLAYER_3",
				},
			},
		}, nil,
		).Once()
	defer games.AssertExpectations(t)
	tiles := new(MockTileRepository)
	tiles.
		On("GetById", mock.Anything, mock.Anything).
		Return(&values.Tile{
			TraversalCost: 1,
		}, nil,
		).Times(3)
	defer tiles.AssertExpectations(t)
	unit := &entities.Unit{
		CurrentTileCoordinates:  "1",
		RemainingMovementPoints: 10,
	}
	units := new(MockUnitRepository)
	units.On("GetById", mock.Anything, mock.Anything).Return(unit, nil).Once()
	defer units.AssertExpectations(t)
	dispatcher := new(MockEventDispatcher)
	dispatcher.
		On("Dispatch", mock.Anything, mock.Anything, mock.Anything).
		Return(nil).
		Once()
	defer dispatcher.AssertExpectations(t)
	payload, err := json.Marshal(&endpoints.MoveUnitRequest{
		PlayerId: "PLAYER_1",
		UnitId:   "UNIT_1",
		Path: []string{
			"1",
			"2",
			"3",
		},
	})
	require.NoError(t, err)
	req, err := http.NewRequest("GET", "/", bytes.NewReader(payload))
	require.NoError(t, err)
	rec := httptest.NewRecorder()
	r := chi.NewMux()
	r.Get("/", endpoints.MoveUnit(games, tiles, units, dispatcher))
	r.ServeHTTP(rec, req)
	var rsp endpoints.MoveUnitResponse
	payload, err = ioutil.ReadAll(rec.Body)
	require.NoError(t, err)
	err = json.Unmarshal(payload, &rsp)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, rec.Code)
	require.Equal(t, "3", unit.CurrentTileCoordinates)
	require.Equal(t, 7, unit.RemainingMovementPoints)
	require.Equal(t, 7, rsp.RemainingMovementPoints)
}
