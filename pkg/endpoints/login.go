package endpoints

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/goombaio/namegenerator"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"math/rand"
	"net/http"
)

type LoginRequest struct {
	ConnectionId string `json:"connection_id"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}

type LoginResponse struct {
	Token       string `json:"token"`
	PlayerName  string `json:"player_name"`
	PlayerId    string `json:"player_id"`
	PlayerLevel int    `json:"player_level"`
}

func Login(generator namegenerator.Generator, players repositories.PlayerRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		request := new(LoginRequest)
		if err := json.Unmarshal(payload, request); err != nil {
			log.Error().Err(err).Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		token := uuid.New().String()
		playerId := uuid.New().String()
		player := &entities.Player{
			Id:           playerId,
			Name:         generator.Generate(),
			Level:        rand.Intn(99) + 1,
			ConnectionId: request.ConnectionId,
		}
		rsp := &LoginResponse{
			Token:       token,
			PlayerName:  player.Name,
			PlayerId:    playerId,
			PlayerLevel: player.Level,
		}
		body, err := json.Marshal(rsp)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if err := players.Add(ctx, player); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(body)
	}
}
