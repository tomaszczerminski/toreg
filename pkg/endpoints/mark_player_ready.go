package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type MarkPlayerReadyRequest struct {
	PlayerID string `json:"player_id"`
	RoomID   string `json:"room_id"`
}

func MarkPlayerReady(
	players repositories.PlayerRepository,
	rooms repositories.RoomRepository,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request MarkPlayerReadyRequest
		if err := json.Unmarshal(body, &request); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room, err := rooms.GetById(ctx, request.RoomID)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var recipients []string
		for _, preference := range room.Preferences {
			if preference.PlayerId == request.PlayerID {
				continue
			}
			recipient, err := players.GetById(ctx, preference.PlayerId)
			if err != nil {
				continue
			}
			recipients = append(recipients, recipient.ConnectionId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.PlayerMarkedReadyEvent{
			PlayerID: request.PlayerID,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(make([]byte, 0))
	}
}
