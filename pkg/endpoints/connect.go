package endpoints

import (
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/caches"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/services"
	"net/http"
	"sync"
)

func Connect(
	connections caches.ConnectionCache,
	watchdog services.Watchdog,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	var upgrader websocket.Upgrader
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		c, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		id := uuid.New().String()
		conn := &entities.Connection{
			Mutex: new(sync.Mutex),
			Conn:  c,
			Id:    id,
		}
		if err := connections.Add(ctx, conn); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if err := watchdog.Watch(ctx, conn); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(make([]byte, 0))
		if err := dispatcher.Dispatch(ctx, []string{id}, &events.PlayerConnectedEvent{
			ConnectionId: id,
		}); err != nil {
			logger.Error().Err(err).Caller().Send()
		}
	}
}
