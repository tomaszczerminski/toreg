package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type ChangeFractionRequest struct {
	PlayerID   string `json:"player_id"`
	FractionID string `json:"fraction_id"`
	RoomID     string `json:"room_id"`
}

type ChangeFractionResponse struct {
	PlayerID   string `json:"player_id"`
	FractionID string `json:"fraction_id"`
}

func ChangeFraction(rooms repositories.RoomRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request ChangeFractionRequest
		if err := json.Unmarshal(payload, &request); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room, err := rooms.GetById(ctx, request.RoomID)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var found bool
		for _, player := range room.Preferences {
			if player.PlayerId == request.PlayerID {
				player.FractionId = request.FractionID
				found = true
				break
			}
		}
		if !found {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		rsp, err := json.Marshal(&ChangeFractionResponse{
			PlayerID:   request.PlayerID,
			FractionID: request.FractionID,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(rsp)
	}
}
