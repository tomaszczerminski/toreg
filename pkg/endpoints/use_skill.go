package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"math/rand"
	"net/http"
)

type UseSkillRequest struct {
	GameId       string `json:"game_id"`
	PlayerId     string `json:"player_id"`
	UnitId       string `json:"unit_id"`
	TargetUnitId string `json:"target_unit_id"`
	SkillId      string `json:"skill_id"`
}

func UseSkill(
	games repositories.GameRepository,
	units repositories.UnitRepository,
	skills repositories.SkillRepository,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		request := new(UseSkillRequest)
		if err := json.Unmarshal(payload, request); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		game, err := games.GetById(ctx, request.GameId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		skill, err := skills.Get(ctx, request.SkillId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		dmg := rand.Intn(skill.MaximumDamage-skill.MinimumDamage) + skill.MinimumDamage
		dead := false
		unit, err := units.GetById(ctx, request.UnitId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		unit.HealthPoints -= dmg
		if unit.HealthPoints <= 0 {
			dead = true
		}
		var recipients []string
		for _, record := range game.Players {
			if record.PlayerId == request.PlayerId {
				continue
			}
			recipients = append(recipients, record.PlayerId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.SkillUsedEvent{
			UnitId:       request.UnitId,
			TargetUnitId: request.TargetUnitId,
			Damage:       dmg,
		}); err != nil {
			return
		}
		if dead {
			if err := dispatcher.Dispatch(ctx, recipients, &events.UnitDiedEvent{
				UnitId: request.UnitId,
			}); err != nil {
				return
			}
		}
	}
}
