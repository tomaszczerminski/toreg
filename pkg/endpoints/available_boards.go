package endpoints

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"net/http"
)

type BoardEntry struct {
	BoardId   string `json:"board_id"`
	BoardName string `json:"board_name"`
	BoardType string `json:"board_type"`
	BoardSize string `json:"board_size"`
}

type GetAvailableBoardsResponse struct {
	Boards []BoardEntry `json:"boards"`
}

func GetAvailableBoards(boards repositories.BoardRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		var entries []BoardEntry
		bb, err := boards.GetAll(ctx)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		for _, board := range bb {
			entries = append(entries, BoardEntry{
				BoardId:   board.Id,
				BoardName: board.Name,
				BoardType: board.Type,
				BoardSize: board.Size,
			})
		}
		rsp, err := json.Marshal(&GetAvailableBoardsResponse{
			Boards: entries,
		})
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(rsp)
	}
}
