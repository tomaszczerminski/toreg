package endpoints

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type CreateRoomRequest struct {
	PlayerId     string `json:"player_id"`
	ConnectionId string `json:"connection_id"`
	BoardId      string `json:"board_id"`
}

type CreateRoomResponse struct {
	RoomId string `json:"room_id"`
}

func CreateRoom(rooms repositories.RoomRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		payload, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		request := new(CreateRoomRequest)
		if err := json.Unmarshal(payload, request); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		id := uuid.New().String()
		if err := rooms.Add(ctx, &entities.Room{
			Id:      id,
			BoardId: request.BoardId,
			Preferences: []*entities.PlayerPreference{
				{
					PlayerId:     request.PlayerId,
				},
			},
		}); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		rsp, err := json.Marshal(&CreateRoomResponse{
			RoomId: id,
		})
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusCreated)
		_, _ = w.Write(rsp)
	}
}
