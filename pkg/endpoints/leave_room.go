package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type LeaveRoomRequest struct {
	PlayerID string `json:"player_id"`
	RoomID   string `json:"room_id"`
}

func LeaveRoom(rooms repositories.RoomRepository, dispatcher events.Dispatcher) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request LeaveRoomRequest
		if err := json.Unmarshal(body, &request); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room, err := rooms.GetById(ctx, request.RoomID)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if err := room.RemovePlayer(ctx, request.PlayerID); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if len(room.Preferences) == 0 {
			_ = rooms.Remove(ctx, request.RoomID)
		}
		var recipients []string
		for _, preference := range room.Preferences {
			if preference.PlayerId == request.PlayerID {
				continue
			}
			recipients = append(recipients, preference.PlayerId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.PlayerLeftRoomEvent{}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(make([]byte, 0))
	}
}
