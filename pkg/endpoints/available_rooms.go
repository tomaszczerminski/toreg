package endpoints

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"net/http"
)

type RoomEntry struct {
	ID        string `json:"room_id"`
	BoardName string `json:"board_name"`
	BoardType string `json:"board_type"`
	OwnerName string `json:"owner_name"`
}

type AvailableRoomsResponse struct {
	Rooms []RoomEntry `json:"rooms"`
}

func GetAvailableRooms(
	rooms repositories.RoomRepository,
	players repositories.PlayerRepository,
	boards repositories.BoardRepository,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		response := &AvailableRoomsResponse{
			Rooms: make([]RoomEntry, 0),
		}
		rr, err := rooms.GetAll(ctx)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		for _, room := range rr {
			if room.IsFull() {
				continue
			}
			owner, err := room.Owner()
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			player, err := players.GetById(ctx, owner)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			board, err := boards.Get(ctx, room.BoardId)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			response.Rooms = append(response.Rooms, RoomEntry{
				ID:        room.Id,
				OwnerName: player.Name,
				BoardName: board.Name,
				BoardType: board.Type,
			})
		}
		rsp, err := json.Marshal(response)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(rsp)
	}
}
