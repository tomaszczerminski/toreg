package endpoints

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type GetPlayersRequest struct {
	GameId string `json:"game_id"`
}

type GetPlayersResponse struct {
	Players []entities.Player `json:"players"`
}

func GetPlayers(games repositories.GameRepository, players repositories.PlayerRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		body, err := r.GetBody()
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		payload, err := ioutil.ReadAll(body)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		request := new(GetPlayersRequest)
		if err := json.Unmarshal(payload, request); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		game, err := games.GetById(ctx, request.GameId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var pp []entities.Player
		for _, record := range game.Players {
			player, err := players.GetById(ctx, record.PlayerId)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
				_, _ = w.Write(make([]byte, 0))
				return
			}
			pp = append(pp, *player)
		}
		rsp, err := json.Marshal(&GetPlayersResponse{
			Players: pp,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(rsp)
	}
}
