package endpoints

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"io/ioutil"
	"net/http"
)

type JoinRoomRequest struct {
	RoomId       string `json:"room_id"`
	PlayerId     string `json:"player_id"`
	ConnectionId string `json:"connection_id"`
}

type PlayerEntry struct {
	PlayerId         string `json:"player_id"`
	PlayerName       string `json:"player_name"`
	PlayerLevel      int    `json:"player_level"`
	SelectedFraction string `json:"selected_fraction"`
}

type JoinGameResponse struct {
	Players []PlayerEntry `json:"players"`
}

func JoinRoom(
	players repositories.PlayerRepository,
	rooms repositories.RoomRepository,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		logger := log.Ctx(ctx)
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request JoinRoomRequest
		if err := json.Unmarshal(body, &request); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room, err := rooms.GetById(ctx, request.RoomId)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if room.IsFull() {
			w.WriteHeader(http.StatusConflict)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		room.Preferences = append(room.Preferences, &entities.PlayerPreference{
			PlayerId:     request.PlayerId,
		})
		entries := make([]PlayerEntry, 0)
		for _, entry := range room.Preferences {
			player, err := players.GetById(ctx, entry.PlayerId)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				_, _ = w.Write(make([]byte, 0))
				return
			}
			entries = append(entries, PlayerEntry{
				PlayerId:    entry.PlayerId,
				PlayerName:  player.Name,
				PlayerLevel: player.Level,
			})
		}
		rsp := &JoinGameResponse{
			Players: entries,
		}
		payload, err := json.Marshal(rsp)
		if err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var recipients []string
		for _, preference := range room.Preferences {
			if preference.PlayerId == request.PlayerId {
				continue
			}
			recipient, err := players.GetById(ctx, preference.PlayerId)
			if err != nil {
				logger.Error().Err(err).Caller().Send()
				continue
			}
			recipients = append(recipients, recipient.ConnectionId)
		}
		player, err := players.GetById(ctx, request.PlayerId)
		if err != nil {

			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.PlayerJoinedRoomEvent{
			PlayerName:  player.Name,
			PlayerLevel: player.Level,
			PlayerID:    player.Id,
			RoomID:      request.RoomId,
		}); err != nil {
			logger.Error().Err(err).Caller().Send()
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(payload)
	}
}
