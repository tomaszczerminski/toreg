package endpoints

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/goombaio/namegenerator"
	"github.com/tomaszczerminski/toreg/pkg/caches"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/factories"
	"github.com/tomaszczerminski/toreg/pkg/middlewares"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"github.com/tomaszczerminski/toreg/pkg/services"
	"time"
)

func Router(
	generator namegenerator.Generator,
	players repositories.PlayerRepository,
	rooms repositories.RoomRepository,
	games repositories.GameRepository,
	units repositories.UnitRepository,
	skills repositories.SkillRepository,
	boards repositories.BoardRepository,
	fractions []entities.Fraction,
	bf factories.BoardFactory,
	connections caches.ConnectionCache,
	dispatcher events.Dispatcher,
	watchdog services.Watchdog,
) *chi.Mux {
	r := chi.NewRouter()
	r.Use(middlewares.Context())
	r.Use(middlewares.Logger())
	r.Use(middleware.Heartbeat("/health"))
	r.Use(middleware.Timeout(60 * time.Second))
	r.Get("/connect", Connect(connections, watchdog, dispatcher))
	r.Get("/available-rooms", GetAvailableRooms(rooms, players, boards))
	r.Get("/available-boards", GetAvailableBoards(boards))
	r.Get("/available-fractions", GetAvailableFractions(fractions))
	r.Get("/available-events", GetAvailableEvents())
	r.Post("/login", Login(generator, players))
	r.Post("/register", Register())
	r.Post("/create-room", CreateRoom(rooms))
	r.Post("/create-game", CreateGame(players, games, units, rooms, fractions, bf, dispatcher))
	r.Post("/join-room", JoinRoom(players, rooms, dispatcher))
	r.Post("/leave-room", LeaveRoom(rooms, dispatcher))
	r.Post("/mark-player-ready", MarkPlayerReady(players, rooms, dispatcher))
	r.Post("/change-fraction", ChangeFraction(rooms))
	r.Post("/move-unit", MoveUnit(games, units, dispatcher))
	r.Post("/use-skill", UseSkill(games, units, skills, dispatcher))
	r.Post("/get-players", GetPlayers(games, players))
	r.Post("/end-turn", EndTurn(games, dispatcher))
	return r
}
