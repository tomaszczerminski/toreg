package endpoints

import (
	"encoding/json"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"github.com/tomaszczerminski/toreg/pkg/values"
	"io/ioutil"
	"net/http"
)

type MoveUnitRequest struct {
	PlayerId string               `json:"player_id"`
	GameId   string               `json:"game_id"`
	UnitId   string               `json:"unit_id"`
	Path     []values.Coordinates `json:"path"`
}

type MoveUnitResponse struct {
	RemainingMovementPoints int `json:"remaining_movement_points"`
}

func MoveUnit(
	games repositories.GameRepository,
	units repositories.UnitRepository,
	dispatcher events.Dispatcher,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		var request MoveUnitRequest
		if err := json.Unmarshal(body, &request); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		game, err := games.GetById(ctx, request.GameId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		unit, err := units.GetById(ctx, request.UnitId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		unit.Lock()
		defer unit.Unlock()
		if unit.CurrentTileCoordinates != request.Path[0] {
			w.WriteHeader(http.StatusBadRequest)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		unit.CurrentTileCoordinates = request.Path[len(request.Path)-1]
		unit.RemainingMovementPoints -= 1
		var recipients []string
		for _, record := range game.Players {
			if record.PlayerId == request.PlayerId {
				continue
			}
			recipients = append(recipients, record.PlayerId)
		}
		if err := dispatcher.Dispatch(ctx, recipients, &events.UnitMovedEvent{
			UnitId: request.UnitId,
			Path:   request.Path,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		rsp, err := json.Marshal(&MoveUnitResponse{
			RemainingMovementPoints: unit.RemainingMovementPoints,
		})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, _ = w.Write(make([]byte, 0))
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(rsp)
	}
}
