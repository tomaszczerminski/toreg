package services

import (
	"context"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/caches"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"time"
)

type Watchdog interface {
	Watch(context.Context, *entities.Connection) error
}

func NewWatchdog(
	games repositories.GameRepository,
	rooms repositories.RoomRepository,
	players repositories.PlayerRepository,
	connections caches.ConnectionCache,
	dispatcher events.Dispatcher,
) *WebsocketWatchdog {
	return &WebsocketWatchdog{
		games:       games,
		rooms:       rooms,
		players:     players,
		connections: connections,
		dispatcher:  dispatcher,
	}
}

type WebsocketWatchdog struct {
	games       repositories.GameRepository
	rooms       repositories.RoomRepository
	players     repositories.PlayerRepository
	connections caches.ConnectionCache
	dispatcher  events.Dispatcher
}

// Watch constantly monitors health of WS connections,
// removing disconnected players and performing other clean-up operations
func (watch *WebsocketWatchdog) Watch(ctx context.Context, conn *entities.Connection) error {
	logger := log.Ctx(ctx)
	signal := make(chan struct{})
	go func() {
		for {
			conn.Lock()
			if err := conn.WriteMessage(websocket.PingMessage, make([]byte, 0)); err != nil {
				logger.Error().Err(err).Caller().Send()
				conn.Unlock()
				return
			}
			conn.Unlock()
			if err := conn.SetReadDeadline(time.Now().Add(5 * time.Second)); err != nil {
				logger.Error().Err(err).Caller().Send()
				return
			}
			if _, _, err := conn.ReadMessage(); err != nil {
				logger.Error().Err(err).Caller().Send()
				return
			}
			signal <- struct{}{}
			<-time.After(5 * time.Second)
		}
	}()
	go func() {
		for {
			select {
			case <-signal:
			case <-time.After(20 * time.Second):
				logger.Info().Caller().Msg("received disconnect signal")
				if err := watch.connections.Remove(ctx, conn.Id); err != nil {
					logger.Error().Err(err).Caller().Msg("cannot remove closed ws connection")
				}
				if err := watch.players.Remove(ctx, conn.Id); err != nil {
					logger.Error().Err(err).Caller().Send()
					return
				}
				game, err := watch.games.GetByPlayerId(ctx, conn.Id)
				if err == nil {
					if err := game.RemovePlayer(ctx, conn.Id); err != nil {
						logger.Error().Err(err).Caller().Send()
						return
					}
					var recipients []string
					for _, ctx := range game.Players {
						if ctx.PlayerId == conn.Id {
							continue
						}
						recipients = append(recipients, ctx.PlayerId)
					}
					if err := watch.dispatcher.Dispatch(ctx, recipients, &events.PlayerLeftGameEvent{
						PlayerId: conn.Id,
					}); err != nil {
						logger.Error().Err(err).Caller().Send()
						return
					}
					if game.IsEmpty() {
						if err := watch.games.Remove(ctx, game.Id); err != nil {
							logger.Error().Err(err).Caller().Send()
							return
						}
					}
					return
				}
				room, err := watch.rooms.GetByPlayerId(ctx, conn.Id)
				if err == nil {
					if err := room.RemovePlayer(ctx, conn.Id); err != nil {
						logger.Error().Err(err).Caller().Send()
						return
					}
					var recipients []string
					for _, preference := range room.Preferences {
						if preference.PlayerId == conn.Id {
							continue
						}
						recipients = append(recipients, preference.PlayerId)
					}
					if err := watch.dispatcher.Dispatch(ctx, recipients, &events.PlayerLeftRoomEvent{
						PlayerId: conn.Id,
					}); err != nil {
						logger.Error().Err(err).Caller().Send()
						return
					}
					if room.IsEmpty() {
						if err := watch.rooms.Remove(ctx, room.Id); err != nil {
							logger.Error().Err(err).Caller().Send()
							return
						}
					}
				}
				return
			}
		}
	}()
	return nil
}
