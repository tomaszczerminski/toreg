package caches

import (
	"context"
	"errors"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"sync"
	"time"
)

type ConnectionCache interface {
	Add(context.Context, *entities.Connection) error
	GetById(context.Context, string) (*entities.Connection, error)
	GetAll(context.Context) ([]*entities.Connection, error)
	Remove(context.Context, string) error
}

func NewConnectionCache(ctx context.Context) *WebsocketConnectionCache {
	cache := &WebsocketConnectionCache{
		Storage: make(map[string]*entities.Connection),
	}
	go func() {
		logger := log.Ctx(ctx)
		for {
			select {
			case <-ctx.Done():
				logger.Info().Msg("received exit signal.")
				return
			case <-time.After(1 * time.Minute):
				cc, err := cache.GetAll(ctx)
				if err != nil {
					continue
				}
				logger.Info().Int("number_of_connections", len(cc)).Msg("connections statistics")
			}
		}
	}()
	return cache
}

type WebsocketConnectionCache struct {
	sync.Mutex
	Storage map[string]*entities.Connection
}

func (cache *WebsocketConnectionCache) Add(_ context.Context, connection *entities.Connection) error {
	cache.Lock()
	defer cache.Unlock()
	cache.Storage[connection.Id] = connection
	return nil
}

var ErrConnectionNotFound = errors.New("connection does not exist")

func (cache *WebsocketConnectionCache) GetById(_ context.Context, id string) (*entities.Connection, error) {
	cache.Lock()
	defer cache.Unlock()
	connection, ok := cache.Storage[id]
	if !ok {
		return nil, ErrConnectionNotFound
	}
	return connection, nil
}

func (cache *WebsocketConnectionCache) GetAll(_ context.Context) ([]*entities.Connection, error) {
	cache.Lock()
	defer cache.Unlock()
	var result []*entities.Connection
	for _, c := range cache.Storage {
		result = append(result, c)
	}
	return result, nil
}

func (cache *WebsocketConnectionCache) Remove(_ context.Context, id string) error {
	cache.Lock()
	defer cache.Unlock()
	delete(cache.Storage, id)
	return nil
}
