package effects

import (
	"context"
	"github.com/tomaszczerminski/toreg/pkg/entities"
)

type Bleeding struct {

}

func (effect *Bleeding) Apply(_ context.Context, unit *entities.Unit) error {
	return nil
}
