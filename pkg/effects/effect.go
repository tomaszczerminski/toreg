package effects

import (
	"context"
	"github.com/tomaszczerminski/toreg/pkg/entities"
)

type Effect interface {
	Apply(context.Context, *entities.Unit) error
}
