package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/goombaio/namegenerator"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tomaszczerminski/toreg/pkg/blueprints"
	"github.com/tomaszczerminski/toreg/pkg/caches"
	"github.com/tomaszczerminski/toreg/pkg/endpoints"
	"github.com/tomaszczerminski/toreg/pkg/entities"
	"github.com/tomaszczerminski/toreg/pkg/events"
	"github.com/tomaszczerminski/toreg/pkg/factories"
	"github.com/tomaszczerminski/toreg/pkg/repositories"
	"github.com/tomaszczerminski/toreg/pkg/services"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

func main() {
	log.Logger.UpdateContext(func(c zerolog.Context) zerolog.Context {
		return c.Str("version", "0.0.1-SNAPSHOT")
	})
	ctx := context.Background()
	ctx = log.Logger.WithContext(ctx)
	log.Logger = log.Output(zerolog.NewConsoleWriter())
	rooms := repositories.NewRoomRepository(ctx)
	players := repositories.NewPlayerRepository(ctx)
	games := repositories.NewGameRepository(ctx)
	units := repositories.NewUnitRepository(ctx)
	skills := repositories.NewSkillRepository()
	boards := repositories.NewBoardRepository()
	if err := setupBoards(boards); err != nil {
		panic(err)
	}
	bf := &factories.DefaultBoardFactory{
		Boards: boards,
	}
	fractions, err := fractions()
	if err != nil {
		panic(err)
	}
	connections := caches.NewConnectionCache(ctx)
	dispatcher := events.NewDispatcher(connections)
	watchdog := services.NewWatchdog(games, rooms, players, connections, dispatcher)
	generator := namegenerator.NewNameGenerator(time.Now().UTC().UnixNano())
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	r := endpoints.Router(
		generator,
		players,
		rooms,
		games,
		units,
		skills,
		boards,
		fractions,
		bf,
		connections,
		dispatcher,
		watchdog,
	)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), r); err != nil {
		panic(err)
	}
}

func setupBoards(boards repositories.BoardRepository) error {
	if err := filepath.Walk("boards", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		var board blueprints.BoardBlueprint
		if err := json.Unmarshal(data, &board); err != nil {
			return err
		}
		return boards.Add(context.Background(), board)
	}); err != nil {
		return err
	}
	return nil
}

func fractions() ([]entities.Fraction, error) {
	fractions := make([]entities.Fraction, 0)
	if err := filepath.Walk("fractions", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		data, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		var fraction entities.Fraction
		if err := json.Unmarshal(data, &fraction); err != nil {
			return err
		}
		fractions = append(fractions, fraction)
		return nil
	}); err != nil {
		return nil, err
	}
	return fractions, nil
}
